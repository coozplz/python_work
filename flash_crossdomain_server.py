#!/usr/bin/python
#-*-coding: UTF-8 -*-
"""
# Data: 2014. 09. 03
# Author: coozplz
# Desc: Embedded Flash 에서 Java Server 로  TCP 연결을 하여 
        통신을 하는데 크로스 도메인 문제로 인해 연결이 약 3초후 자동으로 종료되는
        문제가 발생됨. (SecurityError #2048 번)

        크로스 도메인 서버를 만들기 위해 아래 코드를 작성함.
        
        [서버 기능]
        1. 서버는 843번 포트로 바인딩(Binding) 된다.
        2. 서버는 다수의 클라이언트 접속을 허용한다.
        3. 서버는 클라이언트가 접속하면 'resource/crossdomain.file' 파일을 읽어
           클라이언트로 전송한다.
        4. 전송이 완료되면 연결을 종료한다.

"""
 
import socket
import sys
import time
 
HOST = ''   # Symbolic name, meaning all available interfaces
PORT = 843 # Arbitrary non-privileged port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
print'Socket created'
 
# 서버를 포트와 함께 바인딩 한다. 
# HOST 변수에 값을 넣지 않게 되면 모든 네트워크 인터페이스에 대해 
# Listen이 된다.
try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
    sys.exit()
     
print 'Socket bind complete'
 
# 
#  클라이언트 연결을 대기한다.
# 
s.listen(10)
print 'Socket now listening'
 
msg = ''
f = open('resources/crossdomain.file', 'r')
lines = f.readlines()
for line in lines:
    msg += str(line)
#now keep talking with the client
while 1:

    # 클라이언트 연결을 대기한다.
    conn, addr = s.accept()
    print 'Connected: ' + addr[0] + ':' + str(addr[1])
    conn.sendall(msg)
    print 'SEND OK!! ' + addr[0]
    time.sleep(10)
    conn.sendall(msg)
    conn.close()
    print 'SEND OK!! ' + addr[0]
s.close()
