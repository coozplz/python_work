#-*- coding: utf-8 -*-
# ===============================================================
# Author:   coozplz@gmail.com
# File:     find_missing_number.py
# Date:     2015. 07. 30
# Desc:     한글 컨텐츠 mp3 파일명 변경 프로그램
# ===============================================================

import os


def rename_sound_content(path, key):
	for fileitem in os.listdir(path):
		index = fileitem[5:8]
		origin_file_path = os.path.join(path, fileitem)		
		rename_file_name = "%s%s_%s.mp3" % (key, index, index)
		rename_file_path = os.path.join(path, rename_file_name)		
		os.rename(origin_file_path, rename_file_path)
		print "%s to %s" % (fileitem, rename_file_name)
	

if __name__ == '__main__':
	path = "D:/git/flash_korean_novice/resources/mp3/adjective"	
	rename_sound_content(path, 'A')	
	
