__author__ = 'coozplz'

import io


class FileUtils(object):

    @staticmethod
    def write(file_name, content):
        """
        Write xml content to file
        :param file_name: file file name
        :param content: file content
        :return:
        """
        with io.open(file_name, mode='w', encoding='utf-8') as wp:
            wp.write(content)

    @staticmethod
    def read(file_name):
        """
        Load from file file to string
        :param file_name: file file name
        :return:  file content
        """
        with io.open(file_name, mode='r', encoding='utf-8') as fp:
            return fp.read()



