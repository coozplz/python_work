__author__ = 'coozplz'

import xml.etree.ElementTree as etree
from os import listdir
from os.path import isfile, join
import io


class XMLRearranger:
    """
     XML 파일의 SEQ를 변경한다.
    """
    def __init__(self, source_file):
        self.source_file = source_file

    def __writeXML(self, uXmlData):
        '''
        Xml을 파일로 저장한다.
        :param uXmlData:    XML 데이터
        '''
        with io.open(self.source_file, mode='w', encoding='utf-8') as wp:
            wp.write(uXmlData)

    def rearrage(self):
        uXmlData = self.__loadXml()
        '''
        XML 데이터의 id 순서를 재조정한다.
        :param uXmlData: file string
        :return: 조정된 file data
        '''
        tree = etree.fromstring(uXmlData)
        for child in tree.findall('category'):
            i = 0
            for kanziElement in child.findall('kanzi'):
                i += 1
                kanziElement.set('id', str(i))
                # print(kanziElement.attrib)
        bytes_xml = etree.tostring(tree, 'utf-8', method='file')
        str_xml = bytes_xml.decode('utf-8')

        self.__writeXML(str_xml)

    def __loadXml(self):
        '''
        XML 파일을 읽어 String 으로 변환한다.
        :return:
        '''
        with io.open(self.source_file, mode='r', encoding='utf-8') as fp:
            uXmlData = fp.read()
            return uXmlData


def main():
    """
    main 함수
    :return:
    """
    root_file_path = "C:\\Users\\Ohsung\\Downloads\\temp_xml"

    for dir_name in listdir(root_file_path):
        dir_absolute_path = join(root_file_path, str(dir_name))
        for f in listdir(join(root_file_path, str(dir_name))):
            xml_file_path = join(dir_absolute_path, str(f))
            print(xml_file_path)
            # rearrager = XMLRearranger(str(xml_file_path))
            # rearrager.rearrage()
    #
    # rearrager.rearrage()


main()
