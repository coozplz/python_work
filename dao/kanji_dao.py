__author__ = 'coozplz'
import sqlite3


class KanjiDAO(object):
    """
    kanji.sqlite 을 조회한다.
    """

    def __init__(self, db_file_name):
        self.db_file_name = db_file_name

    def __conn(self):
        """
        데이터베이스에 연결을 한다.
        """
        self.conn = sqlite3.connect(self.db_file_name)
        self.cursor = self.conn.cursor()

    def __disconnect(self):
        """
        데이터베이스 연결을 해제.
        """
        if self.conn is None:
            return
        self.conn.close()

    def select_kanzi_code_by_kanzi(self, kanzi_name):
        """
        :param kanzi_name:
        :return:
        """
        self.__conn()
        self.cursor.execute('SELECT KANJI_CODE FROM KANJI_MST WHERE KANJI=:kanji', {"kanji": kanzi_name})
        result_set = self.cursor.fetchone()

        if result_set is None or len(result_set) == 0:
            return None

        for member in result_set:
            return member
        # print(self.cursor.fetchall())
