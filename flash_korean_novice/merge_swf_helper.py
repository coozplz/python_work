# -*- coding: UTF-8 -*-
# ===============================================================
# Author:   coozplz@gmail.com
# File:     find_missing_number.py
# Date:     2015. 08. 05
# Desc:   SWF를 Merge 하기 위한 스크립트를 생성한다.
# ===============================================================

import os
import argparse
import sys
import subprocess
from os import path

# 전역 변수 설정 
PROJECT_HOME = 'C://temp'
CLASS_NAME='Noun'
VAR_PATTERN = "[Embed(source='./{dirItem}/{fileItem}')] private var _{varName}:Class;\n"
METHOD_PATTERN = "public function %s() : MovieClip { var mc:MovieClip = new _%s(); return mc; }\n"


def create_as_template(variables, methods, class_name):
  """
  스크립트 파일을 생성하기 위한 텍스트를 생성한다.
  @param variables  변수로 선언될 항목
  @param methods    함수로 선언될 항목
  @param class_name 클래스명과 생성자에 사용될 항목 
  """
  template = """
              package {
                import flash.display.Sprite;
                import flash.display.*;

                dynamic public class %s extends Sprite {
                
                    %s
                    
                    public function %s() {}
                    
                    %s
                }
              }
              """
  return template % (class_name, variables, class_name, methods)


#make_script
def make_script():
  """
  SWF파일을 포함하는 스크립트를 생성한다.
  """    
  for dirItem in os.listdir(PROJECT_HOME):
    if path.isdir(path.join(PROJECT_HOME, dirItem)):
      output_file = '%s.as' % dirItem
      var_array = []
      method_array = []    
      # noun, Adjective, Number를 출력한다.
      for fileItem in os.listdir(path.join(PROJECT_HOME, dirItem)):
        # fileItem = 001-N001_a.swf
        try:                
          if (fileItem.index('.swf') > 5):
            # 001-N001_a.swf => N001_a 로 변환 
            varName = fileItem[4:10]
            var_message = VAR_PATTERN.format(dirItem=dirItem, fileItem=fileItem, varName=varName)
            method_message = METHOD_PATTERN % (varName, varName)      
            var_array.append(var_message)
            method_array.append(method_message)
        except Exception, e:
          print e
          print 'Error occurred'
      script_contents = create_as_template(''.join(var_array), ''.join(method_array), dirItem)
      write_to_file(script_contents, output_file)


def write_to_file(text_content, fileName):
  """
  텍스트를 파일로 저장한다.
  """
  output_file = open('%s/%s' % (PROJECT_HOME, fileName), 'w')
  output_file.write(text_content)
  output_file.close()
  print "File write successfully, fileName=%s" % (output_file)

 
#
# 메인 함수 
#
if __name__ == '__main__':  
  make_script()
  
  