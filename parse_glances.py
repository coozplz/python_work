import argparse

#입력 파라미터
parser = argparse.ArgumentParser()
parser.add_argument('fileName', help='File name for parsing', type=str)
args = parser.parse_args()
FILE_NAME = args.fileName

class CpuInfo:
    system = 0
    user = 0
    idle = 0


class MemInfo:
    available = 0
    used = 0
    percent = 0


f = open(FILE_NAME)

infoArray = []

i = 1
for readStr in f:
    if i % 4 == 0:
        dataArray = readStr.split(",")
        if i % 8 == 0:
            cInfo = CpuInfo()
            cInfo.system = dataArray[2]
            cInfo.idle = dataArray[4]
            cInfo.user = dataArray[5]
            infoArray.append(cInfo)
        else:
            mInfo = MemInfo()
            mInfo.available = dataArray[0]
            mInfo.used = dataArray[1]
            mInfo.percent = dataArray[2]
            infoArray.append(mInfo)
    i += 1

cInfoCount = 0
sum_cpu_system = 0.0
sum_cpu_idle = 0.0
sum_cpu_user = 0.0

mInfoCount = 0
sum_mem_available = 0
sum_mem_used = 0

for obj in infoArray:
    if isinstance(obj, CpuInfo):
        sum_cpu_system += float(obj.system)
        sum_cpu_idle += float(obj.idle)
        sum_cpu_user += float(obj.user)
        cInfoCount += 1
    elif isinstance(obj, MemInfo):
        sum_mem_available += float(obj.available)
        sum_mem_used += float(obj.used)
        mInfoCount += 1
        # print('Available', obj.available)

print('CPU,count=%d system=%.2f, idle=%.2f, user=%.2f' % (cInfoCount
                                                          , sum_cpu_system / cInfoCount
                                                          , sum_cpu_idle / cInfoCount
                                                          , sum_cpu_user / cInfoCount))
print('Memory, count=%d, available=%d MBytes, used=%d MBytes' % (mInfoCount
                                                   , sum_mem_available / mInfoCount / 1024 /1024
                                                   , sum_mem_used / mInfoCount / 1024 / 1024))